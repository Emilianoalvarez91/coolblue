//
//  MockRatingModelMapper.swift
//  CoolblueTests
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
@testable import Coolblue

class MockRatingModelMapper: RatingModelMapperProtocol {

  var mapCounter = 0
  var ratingProductModel: RatingProductModel!

  func map(reviewAverage: Double, reviewCount: Int) -> RatingProductModel {
    mapCounter += 1
    return ratingProductModel
  }
}

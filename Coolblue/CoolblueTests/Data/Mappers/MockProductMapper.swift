//
//  MockProductMapper.swift
//  CoolblueTests
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
@testable import Coolblue

class MockProductMapper: ProductMapperProtocol {

  var product: Product!
  var productList: [Product]!
  var mapCounter = 0
  var mapListCounter = 0

  func map(productModel: ProductModel) -> Product {
    mapCounter += 1
    return product
  }

  func map(productListModel: ProductListModel) -> [Product] {
    mapListCounter += 1
    return productList
  }
}

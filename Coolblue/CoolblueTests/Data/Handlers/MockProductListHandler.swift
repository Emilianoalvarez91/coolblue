//
//  MockProductListHandler.swift
//  CoolblueTests
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
@testable import Coolblue

class MockProductListHandler: ProductListHandlerProtocol {

  var isFetchSuccessful: Bool = false
  var productListModel: ProductListModel?
  var productDetailListModel: ProductDetailListModel?
  var apiResponseError: APIResponseError?
  var fetchProductListCounter = 0
  var fetchProductDetailCounter = 0

  func fetchProductList(query: String, page: Int, onSuccess: @escaping (ProductListModel) -> Void, onError: @escaping (APIResponseError) -> Void) {
    if isFetchSuccessful {
      onSuccess(productListModel!)
    } else {
      onError(apiResponseError!)
    }
    fetchProductListCounter += 1
  }

  func fetchProductDetail(productId: String, onSuccess: @escaping (ProductDetailListModel) -> Void, onError: @escaping (APIResponseError) -> Void) {
    if isFetchSuccessful {
      onSuccess(productDetailListModel!)
    } else {
      onError(apiResponseError!)
    }
    fetchProductDetailCounter += 1
  }
}

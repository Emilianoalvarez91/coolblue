//
//  MockProductListModel.swift
//  CoolblueTests
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
@testable import Coolblue

class MockProductListModel {

  static var reviewSummaryModel = ReviewSummaryModel(
    reviewAverage: 0,
    reviewCount: 0
  )

  static var reviewInformationModel = ReviewInformationModel(reviewSummary: reviewSummaryModel)

  static var productModel = ProductModel(
    productId: "productId",
    productName: "productName",
    reviewInformation: reviewInformationModel,
    usps: nil,
    availabilityState: 0,
    salesPriceIncVat: 0,
    listPriceIncVat: nil,
    listPriceExVat: nil,
    productImage: nil,
    coolbluesChoiceInformationTitle: nil,
    promoIcon: nil,
    nextDayDelivery: false
  )

  static var productListMock = ProductListModel(
    products: [productModel],
    currentPage: 0,
    pageSize: 0,
    totalResults: 0,
    pageCount: 0
  )
}

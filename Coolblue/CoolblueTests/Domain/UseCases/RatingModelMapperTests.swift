//
//  RatingModelMapperTests.swift
//  CoolblueTests
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import XCTest
@testable import Coolblue

class RatingModelMapperTests: XCTestCase {

  let star0ImageName = "Star0"
  let star1ImageName = "Star1"
  let star2ImageName = "Star2"
  let star3ImageName = "Star3"
  let star4ImageName = "Star4"
  let star5ImageName = "Star5"
  var ratingModelMapper: RatingModelMapper!

  override func setUp() {
    super.setUp()
    ratingModelMapper = RatingModelMapper()
  }

  // MARK: - Map Tests
  func testMap_with0ReviewCounts_shouldSetupEmptyCounter() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0, reviewCount: 0)

    // Verify
    XCTAssertEqual(String(), ratingModelProduct.ratingCounterText)
  }

  func testMap_withMoreThan0ReviewCounts_shouldDisplayReviewCounts() {
    // Setup
    let expectedReviewCountText = "(1)"

    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0, reviewCount: 1)

    // Verify
    XCTAssertEqual(expectedReviewCountText, ratingModelProduct.ratingCounterText)
  }

  func testMap_with0Rating_shouldSetupAllZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with0_1Rating_shouldSetup1PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0.1, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star1ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with0_2Rating_shouldSetup1PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0.2, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star1ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with0_3Rating_shouldSetup1PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0.3, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star1ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with0_4Rating_shouldSetup2PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0.4, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with0_5Rating_shouldSetup2PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0.5, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with0_6Rating_shouldSetup2PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0.6, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with0_7Rating_shouldSetup2PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0.7, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with0_8Rating_shouldSetup3PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0.8, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star3ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with0_9Rating_shouldSetup3PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 0.9, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star3ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with1_0Rating_shouldSetup3PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 1.0, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star3ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with1_1Rating_shouldSetup3PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 1.1, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star3ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with1_2Rating_shouldSetup4PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 1.2, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star4ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with1_3Rating_shouldSetup4PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 1.3, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star4ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with1_4Rating_shouldSetup4PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 1.4, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star4ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with1_5Rating_shouldSetup4PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 1.5, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star4ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with1_6Rating_shouldSetup4PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 1.6, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with1_7Rating_shouldSetup1FullStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 1.7, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with1_8Rating_shouldSetup1FullStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 1.8, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with1_9Rating_shouldSetup1FullStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 1.9, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with2_0Rating_shouldSetup1FullStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 2.0, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with2_1Rating_shouldSetup1FullStarAnd1PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 2.1, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star1ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with2_2Rating_shouldSetup1FullStarAnd1PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 2.2, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star1ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with2_3Rating_shouldSetup1FullStarAnd1PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 2.3, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star1ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with2_4Rating_shouldSetup1FullStarAnd2PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 2.4, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star1ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with2_5Rating_shouldSetup1FullStarAnd2PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 2.5, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with2_6Rating_shouldSetup1FullStarAnd2PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 2.6, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with2_7Rating_shouldSetup1FullStarAnd3PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 2.7, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with2_8Rating_shouldSetup1FullStarAnd3PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 2.8, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with2_9Rating_shouldSetup1FullStarAnd3PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 2.9, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star3ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with3_0Rating_shouldSetup1FullStarAnd3PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 3.0, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star3ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with3_1Rating_shouldSetup1FullStarAnd3PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 3.1, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star3ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with3_2Rating_shouldSetup1FullStarAnd4PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 3.2, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star4ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with3_3Rating_shouldSetup1FullStarAnd4PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 3.3, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star4ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with3_4Rating_shouldSetup1FullStarAnd4PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 3.4, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star4ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with3_5Rating_shouldSetup1FullStarAnd4PointStarImageAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 3.5, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star4ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with3_6Rating_shouldSetup2FullStarAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 3.6, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with3_7Rating_shouldSetup1FullStarAnd5PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 3.7, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with3_8Rating_shouldSetup1FullStarAnd5PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 3.8, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with3_9Rating_shouldSetup1FullStarAnd5PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 3.9, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with4_0Rating_shouldSetup1FullStarAnd5PointStarImageAnd4ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 4.0, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with4_1Rating_shouldSetup2FullStarsAnd1PointStarAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 4.1, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star1ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with4_2Rating_shouldSetup2FullStarsAnd1PointStarAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 4.2, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star1ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with4_3Rating_shouldSetup2FullStarsAnd1PointStarAnd3ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 4.3, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star1ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with4_4Rating_shouldSetup2FullStarsAnd1PointStarAnd2ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 4.4, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with4_5Rating_shouldSetup2FullStarsAnd2PointStarImageAnd2ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 4.5, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with4_6Rating_shouldSetup2FullStarsAnd2PointStarImageAnd2ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 4.6, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with4_7Rating_shouldSetup2FullStarsAnd2PointStarImageAnd2ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 4.7, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with4_8Rating_shouldSetup2FullStarsAnd3PointStarImageAnd2ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 4.8, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star2ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with4_9Rating_shouldSetup2FullStarsAnd3PointStarImageAnd2ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 4.9, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star3ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }

  func testMap_with5_0Rating_shouldSetup2FullStarsAnd3PointStarImageAnd2ZeroStarImages() {
    // Test
    let ratingModelProduct = ratingModelMapper.map(reviewAverage: 5.0, reviewCount: 0)

    // Verify
    XCTAssertEqual(ratingModelProduct.star1ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star2ImageName, star5ImageName)
    XCTAssertEqual(ratingModelProduct.star3ImageName, star3ImageName)
    XCTAssertEqual(ratingModelProduct.star4ImageName, star0ImageName)
    XCTAssertEqual(ratingModelProduct.star5ImageName, star0ImageName)
  }
}

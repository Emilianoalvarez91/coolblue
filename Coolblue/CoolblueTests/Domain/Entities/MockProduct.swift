//
//  MockProduct.swift
//  CoolblueTests
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
@testable import Coolblue

class MockProduct  {

  static var product = Product(
    id: "id",
    name: "name",
    reviewAverage: 0,
    reviewCount: 0,
    stock: 0,
    price: 0,
    image: String(),
    isNextDayShippable: false,
    type: ProductType.common
  )
}

//
//  LandingViewPresenterTests.swift
//  CoolblueTests
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import XCTest
@testable import Coolblue

class LandingViewPresenterTests: XCTestCase {

  let productList = MockProductListModel.productListMock
  let product = MockProduct.product
  let view = MockLandingViewProtocol()
  let productListHandler = MockProductListHandler()
  let productMapper = MockProductMapper()
  let ratingModelMapper = MockRatingModelMapper()
  var presenter: LandingViewPresenter!

  override func setUp() {
    super.setUp()
    presenter = LandingViewPresenter(view: view, productListHandler: productListHandler, productMapper: productMapper, ratingModelMapper: ratingModelMapper)
  }

  // MARK: - FetchProducts Tests
  func testSearch_withSuccessfulCallback_shouldReturnProductList() {
    // Setup
    productListHandler.isFetchSuccessful = true
    productListHandler.productListModel = productList
    productMapper.productList = [product]

    // Test
    presenter.search(query: "test")

    // Verify
    XCTAssertTrue(!presenter.productList.isEmpty, "Product list shouldn't be empty if handler returns products.")
    XCTAssertEqual(1, productListHandler.fetchProductListCounter, "Fetch product list should have been called.")
    XCTAssertEqual(1, productMapper.mapListCounter, "Map list should have been called.")
    XCTAssertEqual(1, view.reloadDataCounter, "Reload data should have been called.")
  }

  func testSearch_withFailureCallback_shouldNotReturnProductList() {
    // Setup
    productListHandler.isFetchSuccessful = false
    productListHandler.apiResponseError = APIResponseError.malformedData

    // Test
    presenter.search(query: "test")

    // Verify
    XCTAssertTrue(presenter.productList.isEmpty, "Product list should be empty if handler returns an error.")
    XCTAssertEqual(1, productListHandler.fetchProductListCounter, "Fetch product list should have been called.")
    XCTAssertEqual(0, productMapper.mapListCounter, "Map list shouldn't have been called.")
    XCTAssertEqual(0, view.reloadDataCounter, "Reload data shouldn't have been called.")
  }
}

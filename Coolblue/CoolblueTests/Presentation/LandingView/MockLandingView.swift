//
//  MockLandingView.swift
//  CoolblueTests
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
@testable import Coolblue

class MockLandingViewProtocol: LandingViewProtocol {

  var reloadDataCounter = 0
  var showLoadingCounter = 0
  var hideLoadingCounter = 0

  func reloadData() {
    reloadDataCounter += 1
  }

  func showLoading() {
    showLoadingCounter += 1
  }

  func hideLoading() {
    hideLoadingCounter += 1
  }
}

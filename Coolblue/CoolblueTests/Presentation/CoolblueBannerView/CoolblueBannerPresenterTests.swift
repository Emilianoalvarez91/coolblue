//
//  CoolblueBannerPresenterTests.swift
//  CoolblueTests
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import XCTest
@testable import Coolblue

class CoolblueBannerPresenterTests: XCTestCase {

  let view = MockCoolblueBannerView()
  let expectedTitle = "anything for a smile"
  let expectedBoldWord = "smile"

  // MARK: - Test Setup
  func testSetup_withBoldWordContainedInText_shouldSetupViewWithBoldText() {
    // Setup
    let expectedRange = NSRange(location: 15, length: 5)
    let presenter = CoolblueBannerPresenter(view: view, title: expectedTitle, boldWord: expectedBoldWord)

    // Test
    presenter.setup()

    // Verify
    XCTAssertEqual(0, view.setupViewCounter, "SetupView shouldn't get called when the expected bold word is included in the title.")
    XCTAssertEqual(1, view.setupViewWithBoldWordCounter, "SetupView with bold range should get called when the expected bold word is included in the title.")
    XCTAssertEqual(expectedTitle, view.title, "Expected title is not accurate.")
    XCTAssertEqual(expectedRange, view.boldRange, "Bold word range is not accurate.")
  }

  func testSetup_withNoBoldWordContainedInText_shouldSetupViewWithoutBoldText() {
    // Setup
    let expectedBoldWord = "inexistent"
    let presenter = CoolblueBannerPresenter(view: view, title: expectedTitle, boldWord: expectedBoldWord)

    // Test
    presenter.setup()

    // Verify
    XCTAssertEqual(1, view.setupViewCounter, "SetupView should get called when the expected bold word is not included in the title.")
    XCTAssertEqual(0, view.setupViewWithBoldWordCounter, "SetupView with bold range shouldn't get called when the expected bold word is not included in the title.")
    XCTAssertEqual(expectedTitle, view.title, "Expected title is not accurate.")
    XCTAssertNil(view.boldRange, "Bold word range should not change.")
  }

  // MARK: - Test ViewDidAppear
  func testViewDidAppear_shouldAnimateParticleAlways() {
    // Setup
    let presenter = CoolblueBannerPresenter(view: view, title: expectedTitle, boldWord: expectedBoldWord)

    // Test
    presenter.viewDidAppear()
    presenter.viewDidAppear()

    // Verify
    XCTAssertEqual(2, view.animateBannerParticleCounter, "Particle animation should display always when the view appeared.")
  }
}

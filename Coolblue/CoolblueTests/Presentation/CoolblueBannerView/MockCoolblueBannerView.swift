//
//  MockCoolblueBannerView.swift
//  CoolblueTests
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
@testable import Coolblue

class MockCoolblueBannerView: CoolblueBannerViewControllerProtocol {

  var title: String?
  var boldRange: NSRange?
  var setupViewCounter = 0
  var setupViewWithBoldWordCounter = 0
  var animateBannerParticleCounter = 0

  func setupView(title: String) {
    self.title = title
    setupViewCounter += 1
  }

  func setupView(title: String, boldRange: NSRange) {
    self.title = title
    self.boldRange = boldRange
    setupViewWithBoldWordCounter += 1
  }

  func animateBannerParticle() {
    animateBannerParticleCounter += 1
  }
}

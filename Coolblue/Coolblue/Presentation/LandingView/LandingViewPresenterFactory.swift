//
//  LandingViewPresenterFactory.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

class LandingViewPresenterFactory {

  private(set) var presenter: LandingViewPresenterProtocol!

  init(view: LandingViewProtocol) {
    let productListHandler = ProductListHandler()
    let productMapper = ProductMapper()
    let ratingModelMapper = RatingModelMapper()
    presenter = LandingViewPresenter(view: view, productListHandler: productListHandler, productMapper: productMapper, ratingModelMapper: ratingModelMapper)
  }
}

//
//  LandingViewPresenter.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import CoreGraphics

protocol LandingViewPresenterProtocol: SearchableProtocol, TableViewConfigurable {

  func productId(at indexPath: IndexPath) -> String
}

class LandingViewPresenter: LandingViewPresenterProtocol {

  // MARK: - Variables
  private(set) unowned var view: LandingViewProtocol
  private let productListHandler: ProductListHandlerProtocol
  private let productMapper: ProductMapperProtocol
  private let ratingModelMapper: RatingModelMapperProtocol
  private var pageNumber: Int
  private(set) var productList = [Product]()
  private(set) var totalProducts: Int
  private(set) var query: String?
  private(set) var isFetchingNextPage = false

  init(view: LandingViewProtocol, productListHandler: ProductListHandlerProtocol, productMapper: ProductMapperProtocol, ratingModelMapper: RatingModelMapperProtocol) {
    self.view = view
    self.productListHandler = productListHandler
    self.productMapper = productMapper
    self.ratingModelMapper = ratingModelMapper
    pageNumber = 1
    totalProducts = 0
  }

  func productId(at indexPath: IndexPath) -> String {
    return productList[indexPath.row].id
  }

  private func fetchProducts(shouldShowLoading: Bool) {
    guard let query = query,
      !query.isEmpty else {
        productList.removeAll()
        view.reloadData()
        return
    }

    if (shouldShowLoading) {
      view.showLoading()
    }

    isFetchingNextPage = true
    productListHandler.fetchProductList(
      query: query,
      page: pageNumber,
      onSuccess: { [weak self] (productListModel) in
        guard let strongSelf = self else {
          return
        }
        strongSelf.totalProducts = productListModel.totalResults
        strongSelf.isFetchingNextPage = false
        let mappedProductList = strongSelf.productMapper.map(productListModel: productListModel)
        strongSelf.productList.append(contentsOf: mappedProductList)
        strongSelf.view.reloadData()

        if (shouldShowLoading) {
          strongSelf.view.hideLoading()
        }
      },
      onError: { (error) in
        // TODO: - Handle error
      }
    )
  }

  private func fetchNextProducts() {
    guard !isFetchingNextPage else {
      return
    }
    pageNumber += 1
    fetchProducts(shouldShowLoading: false)
  }
}

// MARK: - SearchableProtocol
extension LandingViewPresenter: SearchableProtocol {

  func search(query: String?) {
    self.query = query
    pageNumber = 1
    productList.removeAll()
    fetchProducts(shouldShowLoading: true)
  }
}

// MARK: - TableViewConfigurable
extension LandingViewPresenter: TableViewConfigurable {

  func heightForRow(at indexPath: IndexPath) -> CGFloat {
    return 150
  }

  func numberOfRows(in section: Int) -> Int {
    return totalProducts
  }

  func prefetchRows(at indexPaths: [IndexPath]) {
    let needsFetch = indexPaths.contains {$0.row >= productList.count}
    if needsFetch {
      fetchNextProducts()
    }
  }

  func setup(cell: TableViewCellProcotol, at indexPath: IndexPath) {
    guard let cell = cell as? LandingViewTableViewCellConfigurableProtocol,
          productList.count > indexPath.row else {
      return
    }

    let product = productList[indexPath.row]
    let imageURLString = product.image
    let name = product.name
    let price = String(format: Constants.priceFormat, product.price)
    let currencyImage = ImageConstants.euroCurrency
    let isNextDayShippable = product.isNextDayShippable
    let ratingModel = ratingModelMapper.map(reviewAverage: product.reviewAverage, reviewCount: product.reviewCount)
    cell.setupProduct(imageURLString: imageURLString, name: name, price: price, currencyImageName: currencyImage, ratingModel: ratingModel, isNextDayShippable: isNextDayShippable)

    switch product.type {
      case .discount:
        let discountProduct = product as! DiscountPromoProduct
        let oldPrice = String(format: Constants.oldProductPriceFormat, discountProduct.discountPrice)
        cell.setupProduct(oldPrice: oldPrice)
      case .coolblue:
        let coolblueProduct = product as! CoolbluePromoProduct
        let imageName = ImageConstants.Coolblue.normal
        let promoText = coolblueProduct.coolblueDetail
        cell.setupCoolbluePromo(imageName: imageName, promoText: promoText, isSeparatorViewVisible: false)
      default:
        // Do nothing.
        break
    }
  }
}

//
//  LandingViewController.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import UIKit

protocol LandingViewProtocol: class, TableViewManipulableProtocol, ViewLoadingProtocol {}

class LandingViewController: UIViewController, LandingViewProtocol {

  // MARK: - Constants
  private let tableViewCellIdentifier = String(describing: LandingViewTableViewCell.self)
  private let backgroundImageName = "SmilingBackground"
  private let productDetailSegue = "ProductDetailSegue"

  // MARK: - Outlets
  private let refreshControl = UIRefreshControl()
  lazy var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 44))
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

  // MARK: - Variables
  private var presenter: LandingViewPresenterProtocol!

  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    presenter = LandingViewPresenterFactory(view: self).presenter
  }

  private func setupViews() {
    searchBar.delegate = self
    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: searchBar)
    let nibFile = UINib(nibName: String(describing: LandingViewTableViewCell.self), bundle: nil)
    let backgroundImageView = UIImageView(frame: tableView.frame)
    backgroundImageView.image = UIImage(named: backgroundImageName)
    backgroundImageView.contentMode = UIViewContentMode.scaleAspectFill
    tableView.backgroundView = backgroundImageView
    tableView.register(nibFile, forCellReuseIdentifier: tableViewCellIdentifier)
    refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
    refreshControl.tintColor = ColorConstants.Coolblue.orange
    tableView.refreshControl = refreshControl
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard let destination = segue.destination as? ProductDetailViewController,
          let productId = sender as? String else {
      return
    }
    destination.productId = productId
  }

  @objc private func onRefresh() {
    refreshControl.endRefreshing()
    presenter.search(query: searchBar.text)
  }
}

// MARK: - ViewLoadingProtocol
extension LandingViewController: ViewLoadingProtocol {

  func showLoading() {
    activityIndicator.startAnimating()
    tableView.isHidden = true
  }

  func hideLoading() {
    activityIndicator.stopAnimating()
    tableView.isHidden = false
  }
}

// MARK: - TableViewManipulableProtocol
extension LandingViewController: TableViewManipulableProtocol {

  func reloadData() {
    tableView.reloadData()
  }
}

// MARK: - TableViewDataSource
extension LandingViewController: UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter.numberOfRows(in: section)
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdentifier, for: indexPath)
    if let cell = cell as? TableViewCellProcotol {
      presenter.setup(cell: cell, at: indexPath)
    }
    return cell
  }
}

// MARK: - TableViewDelegate
extension LandingViewController: UITableViewDelegate {

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return presenter.heightForRow!(at: indexPath)
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: false)
    let productId = presenter.productId(at: indexPath)
    performSegue(withIdentifier: productDetailSegue, sender: productId)
  }
}

// MARK: - UITableViewDataSourcePrefetching
extension LandingViewController: UITableViewDataSourcePrefetching {

  func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
    presenter.prefetchRows!(at: indexPaths)
  }

  func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {

  }
}

// MARK: - UISearchBarDelegate
extension LandingViewController: UISearchBarDelegate {

  func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
    searchBar.showsCancelButton = true
    return true
  }

  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
    searchBar.showsCancelButton = false
  }

  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
    searchBar.showsCancelButton = false
    presenter.search(query: searchBar.text)
  }
}

//
//  LandingViewTableViewCell.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import UIKit

protocol LandingViewTableViewCellConfigurableProtocol: TableViewCellProcotol {

  func setupProduct(imageURLString: String, name: String, price: String, currencyImageName: String, ratingModel: RatingProductModel, isNextDayShippable: Bool)

  func setupProduct(oldPrice: String)

  func setupCoolbluePromo(imageName: String, promoText: String, isSeparatorViewVisible: Bool)
}

protocol LandingViewTableViewCellProtocol: class {

  func setupNextDayShippingView(imageName: String, promoText: String, isSeparatorViewVisible: Bool)
}

class LandingViewTableViewCell: UITableViewCell {

  // MARK: - Outlets
  @IBOutlet weak var productImageView: UIImageView!
  @IBOutlet weak var productNameLabel: UILabel!
  @IBOutlet weak var productRatingView: RatingProductView!
  @IBOutlet weak var nextDayShippingView: PromoProductView!
  @IBOutlet weak var coolbluePromoView: PromoProductView!
  @IBOutlet weak var productPriceView: PriceProductView!
  @IBOutlet weak var priceTagImageView: UIImageView!
  @IBOutlet weak var separatorView: UIView!

  // MARK: - Constants
  private let priceTagRotateAnimationDuration = 0.2
  private let rotateAnimationRadians = Double.pi/5

  // MARK: - Variables
  private var presenter: SetupableProtocol!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    presenter = LandingViewTableViewPresenter(view: self)
    presenter.setup()
    setupViews()
  }

  private func setupViews() {
    priceTagImageView.image = UIImage(named: ImageConstants.priceTag)
    productNameLabel.font = FontConstants.Helvetica.regular.of(size: 18)
    productNameLabel.textColor = ColorConstants.gray
    separatorView.backgroundColor = ColorConstants.lightGray
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
}

// MARK: - LandingViewTableViewCellProtocol
extension LandingViewTableViewCell: LandingViewTableViewCellProtocol {

  func setupNextDayShippingView(imageName: String, promoText: String, isSeparatorViewVisible: Bool) {
    nextDayShippingView.setupView(imageName: imageName, promoText: promoText, isSeparatorViewVisible: isSeparatorViewVisible)
  }
}

// MARK: - LandingViewTableViewCellConfigurableProtocol
extension LandingViewTableViewCell: LandingViewTableViewCellConfigurableProtocol {

  func setupProduct(imageURLString: String, name: String, price: String, currencyImageName: String, ratingModel: RatingProductModel, isNextDayShippable: Bool) {
    ImageLoader.sharedInstance.loadImage(
      fromURL: imageURLString,
      onComplete: { (image) in
        self.productImageView.image = image
      }, onError: { _ in
        self.productImageView.image = UIImage(named: ImageConstants.Coolblue.gray)
      }, usingCache: true
    )
    productNameLabel.text = name
    productPriceView.setupView(actualPrice: price, currencyImageName: currencyImageName)
    productRatingView.setup(ratingModel: ratingModel)
    nextDayShippingView.isHidden = !isNextDayShippable
    coolbluePromoView.isHidden = true
    priceTagImageView.isHidden = true
  }

  func setupProduct(oldPrice: String) {
    priceTagImageView.isHidden = false
    rotateAnimation(maxRepetitions: 3, counter: 0, angleInverter: -1)
    productPriceView.setupView(oldPrice: oldPrice)
  }

  private func rotateAnimation(maxRepetitions: Int, counter: Int, angleInverter: Double) {
    guard maxRepetitions != counter else {
      UIView.animate(withDuration: priceTagRotateAnimationDuration, animations: {
          self.priceTagImageView.transform = CGAffineTransform.identity
      })
      return
    }

    UIView.animate(
      withDuration: priceTagRotateAnimationDuration,
      animations: {
        self.priceTagImageView.transform = CGAffineTransform(rotationAngle: CGFloat(angleInverter * self.rotateAnimationRadians))
      },
      completion: { _ in
        self.rotateAnimation(maxRepetitions: maxRepetitions, counter: (counter + 1), angleInverter: -angleInverter)
      }
    )
  }

  func setupCoolbluePromo(imageName: String, promoText: String, isSeparatorViewVisible: Bool) {
    coolbluePromoView.isHidden = false
    coolbluePromoView.setupView(imageName: imageName, promoText: promoText, isSeparatorViewVisible: isSeparatorViewVisible)
  }
}

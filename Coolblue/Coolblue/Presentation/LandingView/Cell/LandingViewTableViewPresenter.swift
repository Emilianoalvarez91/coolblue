//
//  LandingViewTableViewPresenter.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

class LandingViewTableViewPresenter: SetupableProtocol {

  // MARK: - Constants
  private let nextDayShippableKey = "LandingViewTableViewCellNextDayShippable"
  
  // MARK: - Variables
  private(set) unowned var view: LandingViewTableViewCellProtocol

  init(view: LandingViewTableViewCellProtocol) {
    self.view = view
  }

  func setup() {
    let imageName = ImageConstants.deliveryTruck
    let promoText = NSLocalizedString(nextDayShippableKey, comment: "Indicates that a product is shippable tomorrow.")
    view.setupNextDayShippingView(imageName: imageName, promoText: promoText, isSeparatorViewVisible: false)
  }
}

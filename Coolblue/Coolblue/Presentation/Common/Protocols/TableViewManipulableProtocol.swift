//
//  TableViewManipulableProtocol.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

protocol TableViewManipulableProtocol {

  func reloadData()
}

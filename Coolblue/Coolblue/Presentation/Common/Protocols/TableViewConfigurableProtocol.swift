//
//  TableViewConfigurableProtocol.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import CoreGraphics

@objc protocol TableViewConfigurable {

  func numberOfRows(in section: Int) -> Int

  func setup(cell: TableViewCellProcotol, at indexPath: IndexPath)

  @objc optional func prefetchRows(at indexPaths: [IndexPath])

  @objc optional func heightForRow(at indexPath: IndexPath) -> CGFloat
}

//
//  ViewLoadingProtocol.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

protocol ViewLoadingProtocol {

  func showLoading()

  func hideLoading()
}

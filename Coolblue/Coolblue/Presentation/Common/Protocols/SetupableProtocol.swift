//
//  SetupableProtocol.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

protocol SetupableProtocol {

  /**
   * A convenient method for configuring an instance.
   */
  func setup()
}

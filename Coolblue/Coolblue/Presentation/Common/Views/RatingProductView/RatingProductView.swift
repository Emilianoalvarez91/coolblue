//
//  RatingProductView.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import UIKit

protocol RatingProductViewProtocol {

  func setup(ratingModel: RatingProductModel)
}

class RatingProductView: UIView {

  // MARK: - Outlets
  @IBOutlet var contentView: UIView!
  @IBOutlet weak var star1ImageView: UIImageView!
  @IBOutlet weak var star2ImageView: UIImageView!
  @IBOutlet weak var star3ImageView: UIImageView!
  @IBOutlet weak var star4ImageView: UIImageView!
  @IBOutlet weak var star5ImageView: UIImageView!
  @IBOutlet weak var ratingCounterLabel: UILabel!


  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }

  private func commonInit() {
    Bundle.main.loadNibNamed(String(describing: RatingProductView.self), owner: self, options: nil)
    addSubview(contentView)
    contentView.frame = bounds
    contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    ratingCounterLabel.font = FontConstants.Helvetica.regular.of(size: 10)
    ratingCounterLabel.textColor = ColorConstants.lightGray
  }
}

// MARK: - RatingProductViewProtocol
extension RatingProductView: RatingProductViewProtocol {

  func setup(ratingModel: RatingProductModel) {
    star1ImageView.image = UIImage(named: ratingModel.star1ImageName)
    star2ImageView.image = UIImage(named: ratingModel.star2ImageName)
    star3ImageView.image = UIImage(named: ratingModel.star3ImageName)
    star4ImageView.image = UIImage(named: ratingModel.star4ImageName)
    star5ImageView.image = UIImage(named: ratingModel.star5ImageName)
    ratingCounterLabel.text = ratingModel.ratingCounterText
  }
}

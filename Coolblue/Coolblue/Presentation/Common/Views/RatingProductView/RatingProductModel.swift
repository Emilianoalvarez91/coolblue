//
//  RatingProductModel.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

struct RatingProductModel {

  let star1ImageName: String
  let star2ImageName: String
  let star3ImageName: String
  let star4ImageName: String
  let star5ImageName: String
  let ratingCounterText: String
}

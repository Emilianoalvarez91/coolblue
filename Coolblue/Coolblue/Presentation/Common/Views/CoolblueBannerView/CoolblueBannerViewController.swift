//
//  CoolblueBannerViewController.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import UIKit
import SpriteKit

protocol CoolblueBannerViewControllerProtocol: class {

  /**
   * Setups view with the given title.
   * - parameter title: The title to display on the banner.
   */
  func setupView(title: String)

  /**
   * Setups view with the given title and bolds the text on the given range.
   * - parameter title: The title to display on the banner.
   * - parameter boldRange: The range of text to be bold.
   */
  func setupView(title: String, boldRange: NSRange)

  /**
   * Animates the particle beneath the banner title.
   */
  func animateBannerParticle()
}

class CoolblueBannerViewController: UIViewController {

  // MARK: - Constants
  private let bannerTitle = NSLocalizedString("CoolblueBannerViewAnythingForASmileTitle", comment: "Coolblue's slogan phrase.")
  private let boldWord = NSLocalizedString("CoolblueBannerViewSmileWord", comment: "Smile word used for highlighting part of the slogan.")
  private let particleFileName = "CoolblueBannerParticle.sks"
  private let particleFadeAnimationDuration = 1.0
  private let particleAnimationDuration = 0.5

  // MARK: - IBOutlets
  @IBOutlet weak var coolblueImageView: UIImageView!
  @IBOutlet weak var bannerLabel: UILabel!
  @IBOutlet weak var particleSKView: SKView!

  // MARK: - Variables
  private var presenter: CoolblueBannerPresenterProtocol!
  private var particleScene: SKScene!

  override func viewDidLoad() {
    super.viewDidLoad()
    setupScene()
    presenter = CoolblueBannerPresenter(view: self, title: bannerTitle, boldWord: boldWord)
    presenter.setup()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    presenter.viewDidAppear()
  }

  private func setupScene() {
    particleScene = SKScene(size: particleSKView.bounds.size)
    particleScene.backgroundColor = .clear
    particleSKView.allowsTransparency = true
    particleSKView.backgroundColor = .clear
    particleSKView.presentScene(particleScene)
  }
}

// MARK: - CoolblueBannerViewControllerProtocol
extension CoolblueBannerViewController: CoolblueBannerViewControllerProtocol {

  func setupView(title: String) {
    bannerLabel.text = title
  }

  func setupView(title: String, boldRange: NSRange) {
    let boldFont = FontConstants.Helvetica.bold.of(size: bannerLabel.font.pointSize)
    let attributedText = NSMutableAttributedString(string: title)
    attributedText.addAttribute(.font, value: boldFont, range: boldRange)
    bannerLabel.attributedText = attributedText
  }

  func animateBannerParticle() {
    guard let emitter = SKEmitterNode(fileNamed: particleFileName) else {
      return
    }
    emitter.alpha = 0
    emitter.position = CGPoint(x: 10, y: particleScene.frame.size.height/2)
    let waitAction = SKAction.wait(forDuration: particleAnimationDuration)
    let fadeInAction = SKAction.fadeIn(withDuration: particleAnimationDuration)
    let fadeOutAction = SKAction.fadeOut(withDuration: particleAnimationDuration)
    let removeFromParent = SKAction.removeFromParent()
    let moveFirstHalfAction = SKAction.moveTo(x: particleScene.frame.size.width / 2, duration: particleAnimationDuration)
    let moveSecondHalfAction = SKAction.moveTo(x: particleScene.frame.size.width - 20, duration: particleAnimationDuration)
    let groupedFirstHalfActions = SKAction.group([fadeInAction, moveFirstHalfAction])
    let groupedSecondHalfActions = SKAction.group([fadeOutAction, moveSecondHalfAction])
    emitter.run(SKAction.sequence([waitAction, groupedFirstHalfActions, groupedSecondHalfActions, removeFromParent]))
    particleScene.addChild(emitter)
  }
}

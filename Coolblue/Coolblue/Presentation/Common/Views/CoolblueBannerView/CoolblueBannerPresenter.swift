//
//  CoolblueBannerPresenter.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

protocol CoolblueBannerPresenterProtocol: SetupableProtocol, ViewAppearingProtocol {}

class CoolblueBannerPresenter: CoolblueBannerPresenterProtocol {

  // MARK: - Variables
  private(set) unowned var view: CoolblueBannerViewControllerProtocol
  private(set) var title: String
  private(set) var boldWord: String

  init(view: CoolblueBannerViewControllerProtocol, title: String, boldWord: String) {
    self.view = view
    self.title = title
    self.boldWord = boldWord
  }

  func setup() {
    var boldWordPresent = false
    title.enumerateSubstrings(in: title.startIndex..<title.endIndex, options: .byWords) {
      (substring, substringRange, _, _) in
      if substring == self.boldWord {
        boldWordPresent = true
        self.view.setupView(title: self.title, boldRange: NSRange(substringRange, in: self.title))
        return
      }
    }

    guard !boldWordPresent else {
      return
    }
    view.setupView(title: title)
  }

  func viewDidAppear() {
    view.animateBannerParticle()
  }
}

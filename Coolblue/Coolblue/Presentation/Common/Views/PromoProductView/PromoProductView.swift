//
//  PromoProductView.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import UIKit

protocol PromoProductViewProtocol {

  func setupView(imageName: String, promoText: String, isSeparatorViewVisible: Bool)
}

class PromoProductView: UIView {

  // MARK: - Outlets
  @IBOutlet var contentView: UIView!
  @IBOutlet weak var promoImageView: UIImageView!
  @IBOutlet weak var promoLabel: UILabel!
  @IBOutlet weak var separatorView: UIView!

  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }

  private func commonInit() {
    Bundle.main.loadNibNamed(String(describing: PromoProductView.self), owner: self, options: nil)
    addSubview(contentView)
    contentView.frame = bounds
    contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    promoLabel.font = FontConstants.Helvetica.regular.of(size: 12)
    promoLabel.textColor = ColorConstants.lightGray
    separatorView.backgroundColor = ColorConstants.lightGray
  }
}

extension PromoProductView: PromoProductViewProtocol {

  func setupView(imageName: String, promoText: String, isSeparatorViewVisible: Bool) {
    promoImageView.image = UIImage(named: imageName)
    promoLabel.text = promoText
    separatorView.isHidden = !isSeparatorViewVisible
  }
}

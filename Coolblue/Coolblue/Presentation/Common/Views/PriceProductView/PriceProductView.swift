//
//  PriceProductView.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import UIKit

protocol PriceProductViewProtocol {

  func setupView(actualPrice: String, currencyImageName: String)

  func setupView(oldPrice: String)
}

class PriceProductView: UIView {

  // MARK: - Outlets
  @IBOutlet var contentView: UIView!
  @IBOutlet weak var oldPriceLabel: UILabel!
  @IBOutlet weak var newPriceLabel: UILabel!
  @IBOutlet weak var currencyImageView: UIImageView!

  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }

  private func commonInit() {
    Bundle.main.loadNibNamed(String(describing: PriceProductView.self), owner: self, options: nil)
    addSubview(contentView)
    contentView.frame = bounds
    contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    oldPriceLabel.font = FontConstants.Helvetica.regular.of(size: 16)
    newPriceLabel.font = FontConstants.Helvetica.regular.of(size: 18)
    newPriceLabel.tintColor = UIColor.black
  }
}

extension PriceProductView: PriceProductViewProtocol {

  func setupView(oldPrice: String) {
    let attributedText = NSMutableAttributedString(string: oldPrice)
    let attributedTextRange = NSMakeRange(0, attributedText.length)
    attributedText.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: attributedTextRange)
    attributedText.addAttribute(NSAttributedStringKey.foregroundColor, value: ColorConstants.lightGray, range: attributedTextRange)
    oldPriceLabel.attributedText = attributedText
  }

  func setupView(actualPrice: String, currencyImageName: String) {
    newPriceLabel.text = actualPrice
    currencyImageView.image = UIImage(named: currencyImageName)
    oldPriceLabel.text = nil
  }
}

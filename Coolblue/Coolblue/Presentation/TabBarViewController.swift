//
//  TabBarViewController.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

  override func viewDidLoad() {
    super.viewDidLoad()
    self.delegate = self
  }
}

// MARK: - UITabBarControllerDelegate
extension TabBarViewController: UITabBarControllerDelegate {

  func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    var selectedViewController = viewController
    if let navigationController = selectedViewController as? UINavigationController,
       let firstChildViewController = navigationController.childViewControllers.first {
      selectedViewController = firstChildViewController
    }
    switch selectedViewController {
      case let viewController as LandingViewController:
        viewController.searchBar.becomeFirstResponder()
      default:
        break
    }
  }
}

//
//  ProductDetailViewController.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import UIKit

protocol ProductDetailViewProtocol: class, ViewLoadingProtocol {

  func setupProduct(imagesURLString: [String], name: String, price: String, currencyImageName: String, ratingModel: RatingProductModel)
}

class ProductDetailViewController: UIViewController {

  // MARK: - Outlets
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var imagesContainerScrollView: UIScrollView!
  @IBOutlet weak var imagesContainerView: UIView!
  @IBOutlet weak var productNameLabel: UILabel!
  @IBOutlet weak var ratingProductView: RatingProductView!
  @IBOutlet weak var priceProductView: PriceProductView!

  // MARK: - Variables
  var productId: String!
  private var presenter: ProductDetailViewPresenterProtocol!

  override func viewDidLoad() {
    super.viewDidLoad()
    presenter = ProductDetailViewPresenterFactory(view: self, productId: productId).presenter
    presenter.setup()
    setupViews()
  }

  private func setupViews() {
    containerView.backgroundColor = ColorConstants.lightestGray
    imagesContainerView.backgroundColor = UIColor.white
    productNameLabel.font = FontConstants.Helvetica.regular.of(size: 25)
    productNameLabel.textColor = ColorConstants.darkGray
  }
}

// MARK: - ViewLoadingProtocol
extension ProductDetailViewController: ViewLoadingProtocol {

  func showLoading() {
    activityIndicator.startAnimating()
    containerView.isHidden = true
  }

  func hideLoading() {
    activityIndicator.stopAnimating()
    containerView.isHidden = false
  }
}

// MARK: - ProductDetailViewProtocol
extension ProductDetailViewController: ProductDetailViewProtocol {

  func setupProduct(imagesURLString: [String], name: String, price: String, currencyImageName: String, ratingModel: RatingProductModel) {
    let containerWidth = Int(view.frame.size.width)
    for (index, imageURLString) in imagesURLString.enumerated() {
      let imageSize = Int(imagesContainerView.frame.size.height)
      let xPosition = (index * containerWidth) + containerWidth/2 - imageSize/2
      let imageView = UIImageView(frame: CGRect(x: xPosition, y: 0, width: imageSize, height: imageSize))
      imageView.contentMode = UIViewContentMode.scaleAspectFit
      ImageLoader.sharedInstance.loadImage(
        fromURL: imageURLString,
        onComplete: { [weak self] (image) in
          imageView.image = image
          self?.imagesContainerView.addSubview(imageView)
        }, onError: { [weak self] _ in
          imageView.image = UIImage(named: ImageConstants.Coolblue.gray)
          self?.imagesContainerView.addSubview(imageView)
        }
      )
    }
    var scrollViewContentSize = imagesContainerScrollView.contentSize
    scrollViewContentSize.width = CGFloat(imagesURLString.count * containerWidth)
    imagesContainerScrollView.contentSize = scrollViewContentSize
    productNameLabel.text = name
    ratingProductView.setup(ratingModel: ratingModel)
    priceProductView.setupView(actualPrice: price, currencyImageName: currencyImageName)
  }
}

//
//  ProductDetailViewPresenter.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

protocol ProductDetailViewPresenterProtocol: SetupableProtocol {}

class ProductDetailViewPresenter: ProductDetailViewPresenterProtocol {

  // MARK: - Variables
  private(set) unowned var view: ProductDetailViewProtocol
  private let productListHandler: ProductListHandlerProtocol
  private let productDetailMapper: ProductDetailMapperProtocol
  private let ratingModelMapper: RatingModelMapperProtocol
  private let productId: String
  private var productDetail: ProductDetail!

  init(view: ProductDetailViewProtocol, productId: String, productListHandler: ProductListHandlerProtocol, productDetailMapper: ProductDetailMapperProtocol, ratingModelMapper: RatingModelMapper) {
    self.view = view
    self.productId = productId
    self.productListHandler = productListHandler
    self.productDetailMapper = productDetailMapper
    self.ratingModelMapper = ratingModelMapper
  }

  func setup() {
    view.showLoading()
    productListHandler.fetchProductDetail(
      productId: productId,
      onSuccess: { [weak self] (productDetailListModel) in
        guard let strongSelf = self else {
          return
        }
        let productDetail = strongSelf.productDetailMapper.map(productDetailListModel: productDetailListModel)
        strongSelf.productDetail = productDetail
        let price = String(format: Constants.priceFormat, productDetail.price)
        let currencyImage = ImageConstants.euroCurrency
        let ratingModel = strongSelf.ratingModelMapper.map(reviewAverage: productDetail.reviewAverage, reviewCount: productDetail.reviewCount)
        strongSelf.view.setupProduct(imagesURLString: productDetail.images, name: productDetail.name, price: price, currencyImageName: currencyImage, ratingModel: ratingModel)
        strongSelf.view.hideLoading()
      },
      onError: { (error) in
        // TODO: - Handle error
      }
    )
  }
}

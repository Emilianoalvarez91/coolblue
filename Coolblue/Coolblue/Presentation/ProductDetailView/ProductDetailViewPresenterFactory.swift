//
//  ProductDetailViewPresenterFactory.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

class ProductDetailViewPresenterFactory {

  private(set) var presenter: ProductDetailViewPresenterProtocol!

  init(view: ProductDetailViewProtocol, productId: String) {
    let productListHandler = ProductListHandler()
    let productDetailMapper = ProductDetailMapper()
    let ratingModelMapper = RatingModelMapper()
    presenter = ProductDetailViewPresenter(view: view, productId: productId, productListHandler: productListHandler, productDetailMapper: productDetailMapper, ratingModelMapper: ratingModelMapper)
  }
}

//
//  FontConstants.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import UIKit

struct FontConstants {

  enum Helvetica: String {

    case regular = "Helvetica"
    case bold = "Helvetica-Bold"

    func of(size: CGFloat) -> UIFont {
      return UIFont(name: self.rawValue, size: size)!
    }
  }
}

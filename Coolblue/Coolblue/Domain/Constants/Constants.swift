//
//  Constants.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

struct Constants {

  static let priceFormat = "%g"
  static let oldProductPriceFormat = "€\(priceFormat)"
}

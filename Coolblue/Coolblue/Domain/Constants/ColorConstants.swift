//
//  ColorConstants.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import UIKit

struct ColorConstants {

  private static let maxRGBValue: CGFloat = 255.0

  // MARK: - Coolblue colors
  enum Coolblue {
    static let blue = UIColor(
      red: 2/maxRGBValue,
      green: 144/maxRGBValue,
      blue: 227/maxRGBValue,
      alpha: 1
    )

    static let orange = UIColor(
      red: 254/maxRGBValue,
      green: 101/maxRGBValue,
      blue: 0,
      alpha: 1
    )
  }
  
  static let darkGray = UIColor(
    red: 71/maxRGBValue,
    green: 71/maxRGBValue,
    blue: 71/maxRGBValue,
    alpha: 1
  )

  static let gray = UIColor(
    red: 104/maxRGBValue,
    green: 104/maxRGBValue,
    blue: 104/maxRGBValue,
    alpha: 1
  )

  static let lightGray = UIColor(
    red: 170/maxRGBValue,
    green: 170/maxRGBValue,
    blue: 170/maxRGBValue,
    alpha: 1
  )

  static let lightestGray = UIColor(
    red: 248/maxRGBValue,
    green: 248/maxRGBValue,
    blue: 248/maxRGBValue,
    alpha: 1
  )
}

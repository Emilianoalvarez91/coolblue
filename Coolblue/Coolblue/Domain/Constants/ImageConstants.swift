//
//  ImageConstants.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

struct ImageConstants {

  static let euroCurrency = "Euro"
  static let priceTag = "PriceTag"
  static let deliveryTruck = "Truck"

  enum Coolblue {
    static let normal = "CoolblueLogo"
    static let gray = "CoolblueLogoGray"
  }

  enum Star {
    static let zero   = "Star0"
    static let one    = "Star1"
    static let two    = "Star2"
    static let three  = "Star3"
    static let four   = "Star4"
    static let five   = "Star5"
  }
}

//
//  DiscountPromoProduct.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

class DiscountPromoProduct: PromoProduct {

  let discountPrice: Double
  let discountPriceNoTax: Double

  init(id: String,
       name: String,
       reviewAverage: Double,
       reviewCount: Int,
       stock: Int,
       price: Double,
       image: String,
       isNextDayShippable: Bool,
       type: ProductType,
       text: String,
       discountPrice: Double,
       discountPriceNoTax: Double) {
    self.discountPrice = discountPrice
    self.discountPriceNoTax = discountPriceNoTax
    super.init(id: id, name: name, reviewAverage: reviewAverage, reviewCount: reviewCount, stock: stock, price: price, image: image, isNextDayShippable: isNextDayShippable, type: type, text: text)
  }
}

//
//  Product.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

enum ProductType {
  case common
  case coolblue
  case discount
}

class Product: Equatable, Hashable {

  let id: String
  let name: String
  let reviewAverage: Double
  let reviewCount: Int
  let stock: Int
  let price: Double
  let image: String
  let isNextDayShippable: Bool
  let type: ProductType

  init(id: String,
       name: String,
       reviewAverage: Double,
       reviewCount: Int,
       stock: Int,
       price: Double,
       image: String,
       isNextDayShippable: Bool,
       type: ProductType) {
    self.id = id
    self.name = name
    self.reviewAverage = reviewAverage
    self.reviewCount = reviewCount
    self.stock = stock
    self.price = price
    self.image = image
    self.isNextDayShippable = isNextDayShippable
    self.type = type
  }

  static func == (lhs: Product, rhs: Product) -> Bool {
    return lhs.id == rhs.id
  }

  var hashValue: Int {
    return id.hashValue
  }
}

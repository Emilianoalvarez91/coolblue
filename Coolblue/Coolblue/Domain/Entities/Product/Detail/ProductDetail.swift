//
//  ProductDetail.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

class ProductDetail: Product {

  let text: String
  let pros: [String]?
  let cons: [String]?
  let images: [String]
  let specifications: [ProductSpecification]

  init(id: String,
       name: String,
       reviewAverage: Double,
       reviewCount: Int,
       stock: Int,
       price: Double,
       image: String,
       isNextDayShippable: Bool,
       type: ProductType,
       text: String,
       pros: [String]?,
       cons: [String]?,
       images: [String],
       specifications: [ProductSpecification]) {
    self.text = text
    self.pros = pros
    self.cons = cons
    self.images = images
    self.specifications = specifications
    super.init(id: id, name: name, reviewAverage: reviewAverage, reviewCount: reviewCount, stock: stock, price: price, image: image, isNextDayShippable: isNextDayShippable, type: type)
  }
}

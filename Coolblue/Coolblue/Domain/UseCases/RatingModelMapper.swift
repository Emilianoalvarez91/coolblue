//
//  RatingModelMapper.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

protocol RatingModelMapperProtocol {

  func map(reviewAverage: Double, reviewCount: Int) -> RatingProductModel
}

class RatingModelMapper: RatingModelMapperProtocol {

  private let maxStarNumber = 5
  private let reviewCountFormat = "(%li)"

  func map(reviewAverage: Double, reviewCount: Int) -> RatingProductModel {
    let halfReviewAverage = reviewAverage/2
    let fullStarIndex = Int(halfReviewAverage)
    let remainder = halfReviewAverage.truncatingRemainder(dividingBy: 1)

    var startImageNameArray = [String](repeating: ImageConstants.Star.zero, count: maxStarNumber)
    for i in 0..<fullStarIndex {
      startImageNameArray[i] = ImageConstants.Star.five
    }

    if remainder != 0 {
      let remainderPercentage = remainder * 100
      switch remainderPercentage {
        case _ where remainderPercentage < 20:
          startImageNameArray[fullStarIndex] = ImageConstants.Star.one
        case _ where remainderPercentage >= 20 && remainderPercentage < 40:
          startImageNameArray[fullStarIndex] = ImageConstants.Star.two
        case _ where remainderPercentage >= 40 && remainderPercentage < 60:
          startImageNameArray[fullStarIndex] = ImageConstants.Star.three
        case _ where remainderPercentage >= 60 && remainderPercentage < 80:
          startImageNameArray[fullStarIndex] = ImageConstants.Star.four
        default:
          startImageNameArray[fullStarIndex] = ImageConstants.Star.five
      }
    }

    var reviewCountText = String()
    if reviewCount != 0 {
      reviewCountText = String(format: reviewCountFormat, reviewCount)
    }
    return RatingProductModel(star1ImageName: startImageNameArray[0], star2ImageName: startImageNameArray[1], star3ImageName: startImageNameArray[2], star4ImageName: startImageNameArray[3], star5ImageName: startImageNameArray[4], ratingCounterText: reviewCountText)
  }
}

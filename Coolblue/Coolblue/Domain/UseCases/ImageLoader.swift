//
//  ImageLoader.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import UIKit

protocol ImageLoaderProtocol {

  func loadImage(fromURL urlString: String, onComplete: @escaping(_ result: UIImage) -> Void, onError: ((_ error: Error?) -> Void)?
  )

  func loadImage(fromURL urlString: String, onComplete: @escaping(_ result: UIImage) -> Void, onError: ((_ error: Error?) -> Void)?, usingCache: Bool)

  func clearCache()
}

class ImageLoader: ImageLoaderProtocol {

  static public let sharedInstance = ImageLoader()

  private init() {
    _ = NotificationCenter.default.addObserver(forName: Notification.Name.UIApplicationDidReceiveMemoryWarning, object: UIApplication.shared, queue: nil, using: { [weak self] _ in
      self?.clearCache()
    })
  }

  deinit {
    NotificationCenter.default.removeObserver(self)
  }

  private var imageCacheDictionary: [String: UIImage] = [:]

  func loadImage(fromURL urlString: String, onComplete: @escaping (_ result: UIImage) -> Void, onError: ((_ error: Error?) -> Void)?) {
    loadImage(fromURL: urlString, onComplete: onComplete, onError: onError, usingCache: false)
  }

  func loadImage(fromURL urlString: String, onComplete: @escaping (_ result: UIImage) -> Void, onError: ((_ error: Error?) -> Void)?, usingCache: Bool) {
    if usingCache, let image = imageCacheDictionary[urlString] {
      DispatchQueue.main.async {
        onComplete(image)
      }
      return
    }

    guard let url = URL(string: urlString) else {
      DispatchQueue.main.async {
        onError?(nil)
      }
      return
    }

    let request = URLRequest(url: url)
    let task = URLSession.shared.dataTask(with: request) { [weak self] (data, _, error) -> Void in
      guard let strongSelf = self, error == nil else {
        DispatchQueue.main.async {
          onError?(error)
        }
        return
      }

      guard let imageData = data as Data?,
        let image = UIImage(data: imageData) else {
          DispatchQueue.main.async {
            onError?(nil)
          }
          return
      }

      strongSelf.imageCacheDictionary[urlString] = image
      DispatchQueue.main.async {
        onComplete(image)
      }
    }
    task.resume()
  }

  @objc func clearCache() {
    imageCacheDictionary.removeAll()
  }
}

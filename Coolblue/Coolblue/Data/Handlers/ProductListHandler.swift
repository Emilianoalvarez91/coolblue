//
//  ProductListHandler.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import Alamofire

protocol ProductListHandlerProtocol {

  func fetchProductList(query: String, page: Int, onSuccess: @escaping (ProductListModel) -> Void, onError: @escaping (APIResponseError) -> Void)

  func fetchProductDetail(productId: String, onSuccess: @escaping (ProductDetailListModel) -> Void, onError: @escaping (APIResponseError) -> Void)
}

class ProductListHandler: ProductListHandlerProtocol {

  private let host = "https://bdk0sta2n0.execute-api.eu-west-1.amazonaws.com/ios-assignment/"

  func fetchProductList(query: String, page: Int, onSuccess: @escaping (ProductListModel) -> Void, onError: @escaping (APIResponseError) -> Void) {
    guard let url = URL(string: "\(host)search?query=\(query)&page=\(page)") else {
      onError(APIResponseError.invalidURL)
      return
    }

    Alamofire.request(url,
                      method: .get,
                      parameters: ["include_docs": "true"])
      .validate()
      .responseJSON { response in
        guard response.result.isSuccess else {
          onError(APIResponseError.error(response.result.error))
          return
        }

        guard let value = response.result.value as? [String: Any] else {
          onError(APIResponseError.malformedData)
          return
        }

        do {
          let products = try ProductListModel(JSON: value)
          onSuccess(products)
        } catch {
          onError(APIResponseError.mappingError)
        }
    }
  }

  func fetchProductDetail(productId: String, onSuccess: @escaping (ProductDetailListModel) -> Void, onError: @escaping (APIResponseError) -> Void) {
    guard let url = URL(string: "\(host)product/\(productId)") else {
      onError(APIResponseError.invalidURL)
      return
    }

    Alamofire.request(url,
                      method: .get,
                      parameters: ["include_docs": "true"])
      .validate()
      .responseJSON { response in
        guard response.result.isSuccess else {
          onError(APIResponseError.error(response.result.error))
          return
        }

        guard let value = response.result.value as? [String: Any] else {
          onError(APIResponseError.malformedData)
          return
        }

        do {
          let product = try ProductDetailListModel(JSON: value)
          onSuccess(product)
        } catch {
          onError(APIResponseError.mappingError)
        }
    }
  }
}

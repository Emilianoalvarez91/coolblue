//
//  ProductModel.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import ObjectMapper

class ProductModel: ImmutableMappable {

  let productId: String
  let productName: String
  let reviewInformation: ReviewInformationModel
  let usps: [String]?
  let availabilityState: Int
  let salesPriceIncVat: Double
  let listPriceIncVat: Double?
  let listPriceExVat: Double?
  let productImage: String?
  let coolbluesChoiceInformationTitle: String?
  let promoIcon: PromoIconModel?
  let nextDayDelivery: Bool

  init(productId: String,
       productName: String,
       reviewInformation: ReviewInformationModel,
       usps: [String]?,
       availabilityState: Int,
       salesPriceIncVat: Double,
       listPriceIncVat: Double?,
       listPriceExVat: Double?,
       productImage: String?,
       coolbluesChoiceInformationTitle: String?,
       promoIcon: PromoIconModel?,
       nextDayDelivery: Bool) {
    self.productId = productId
    self.productName = productName
    self.reviewInformation = reviewInformation
    self.usps = usps
    self.availabilityState = availabilityState
    self.salesPriceIncVat = salesPriceIncVat
    self.listPriceIncVat = listPriceIncVat
    self.listPriceExVat = listPriceExVat
    self.productImage = productImage
    self.coolbluesChoiceInformationTitle = coolbluesChoiceInformationTitle
    self.promoIcon = promoIcon
    self.nextDayDelivery = nextDayDelivery
  }

  required init(map: Map) throws {
    productId                       = try map.value("productId", using: StringTransformable())
    productName                     = try map.value("productName")
    reviewInformation               = try map.value("reviewInformation")
    usps                            = try? map.value("USPs")
    availabilityState               = try map.value("availabilityState")
    salesPriceIncVat                = try map.value("salesPriceIncVat")
    listPriceIncVat                 = try? map.value("listPriceIncVat")
    listPriceExVat                  = try? map.value("listPriceExVat")
    productImage                    = try? map.value("productImage")
    coolbluesChoiceInformationTitle = try? map.value("coolbluesChoiceInformationTitle")
    promoIcon                       = try? map.value("promoIcon")
    nextDayDelivery                 = try map.value("nextDayDelivery")
  }

  func mapping(map: Map) {
    productId                       >>> (map["productId"], StringTransformable())
    productName                     >>> map["productName"]
    reviewInformation               >>> map["reviewInformation"]
    usps                            >>> map["USPs"]
    availabilityState               >>> map["availabilityState"]
    salesPriceIncVat                >>> map["salesPriceIncVat"]
    listPriceIncVat                 >>> map["listPriceIncVat"]
    listPriceExVat                  >>> map["listPriceExVat"]
    productImage                    >>> map["productImage"]
    coolbluesChoiceInformationTitle >>> map["coolbluesChoiceInformationTitle"]
    promoIcon                       >>> map["promoIcon"]
    nextDayDelivery                 >>> map["nextDayDelivery"]
  }
}

//
//  ReviewInformationModel.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import ObjectMapper

struct ReviewInformationModel: ImmutableMappable {

  let reviewSummary: ReviewSummaryModel

  init(reviewSummary: ReviewSummaryModel) {
    self.reviewSummary = reviewSummary
  }

  init(map: Map) throws {
    reviewSummary = try map.value("reviewSummary")
  }

  func mapping(map: Map) {
    reviewSummary >>> map["reviewSummary"]
  }
}

//
//  ReviewSummaryModel.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import ObjectMapper

struct ReviewSummaryModel: ImmutableMappable {

  let reviewAverage: Double
  let reviewCount: Int

  init(reviewAverage: Double,
       reviewCount: Int) {
    self.reviewAverage = reviewAverage
    self.reviewCount = reviewCount
  }
  
  init(map: Map) throws {
    reviewAverage = try map.value("reviewAverage")
    reviewCount   = try map.value("reviewCount")
  }

  func mapping(map: Map) {
    reviewAverage >>> map["reviewAverage"]
    reviewCount   >>> map["reviewCount"]
  }
}

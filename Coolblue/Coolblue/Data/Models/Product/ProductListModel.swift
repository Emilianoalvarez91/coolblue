//
//  ProductListModel.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import ObjectMapper

struct ProductListModel: ImmutableMappable {

  let products: [ProductModel]
  let currentPage: Int
  let pageSize: Int
  let totalResults: Int
  let pageCount: Int

  init(products: [ProductModel],
       currentPage: Int,
       pageSize: Int,
       totalResults: Int,
       pageCount: Int) {
    self.products = products
    self.currentPage = currentPage
    self.pageSize = pageSize
    self.totalResults = totalResults
    self.pageCount = pageCount
  }

  init(map: Map) throws {
    products      = try map.value("products")
    currentPage   = try map.value("currentPage")
    pageSize      = try map.value("pageSize")
    totalResults  = try map.value("totalResults")
    pageCount     = try map.value("pageCount")
  }

  func mapping(map: Map) {
    products      >>> map["products"]
    currentPage   >>> map["currentPage"]
    pageSize      >>> map["pageSize"]
    totalResults  >>> map["totalResults"]
    pageCount     >>> map["pageCount"]
  }
}

//
//  PromoIconModel.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 06/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import ObjectMapper

struct PromoIconModel: ImmutableMappable {

  let text: String
  let type: String

  init(text: String,
       type: String) {
    self.text = text
    self.type = type
  }

  init(map: Map) throws {
    text = try map.value("text")
    type = try map.value("type")
  }

  func mapping(map: Map) {
    text >>> map["text"]
    type >>> map["type"]
  }
}

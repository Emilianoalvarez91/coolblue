//
//  ProductDetailModel.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import ObjectMapper

class ProductDetailModel: ProductModel {

  let productText: String
  let pros: [String]?
  let cons: [String]?
  let productImages: [String]
  let deliveredWith: [String]?
  let specificationSummary: [SpecificationSummaryModel]

  required init(map: Map) throws {
    productText           = try map.value("productText")
    pros                  = try? map.value("pros")
    cons                  = try? map.value("cons")
    productImages         = try map.value("productImages")
    deliveredWith         = try? map.value("deliveredWith")
    specificationSummary  = try map.value("specificationSummary")
    try super.init(map: map)
  }

  override func mapping(map: Map) {
    productText           >>> map["productText"]
    pros                  >>> map["pros"]
    cons                  >>> map["cons"]
    productImages         >>> map["productImages"]
    deliveredWith         >>> map["deliveredWith"]
    specificationSummary  >>> map["specificationSummary"]
    super.mapping(map: map)
  }
}

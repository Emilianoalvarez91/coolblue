//
//  SpecificationSummaryModel.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import ObjectMapper

struct SpecificationSummaryModel: ImmutableMappable {

  let name: String
  let value: String

  init(map: Map) throws {
    name  = try map.value("name")
    value = try map.value("value", using: StringTransformable())
  }

  func mapping(map: Map) {
    name  >>> map["name"]
    value >>> (map["value"], StringTransformable())
  }
}

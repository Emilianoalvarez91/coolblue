//
//  ProductDetailListModel.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import ObjectMapper

struct ProductDetailListModel: ImmutableMappable {

  let product: ProductDetailModel

  init(map: Map) throws {
    product = try map.value("product")
  }

  func mapping(map: Map) {
    product >>> map["product"]
  }
}

//
//  ProductMapper.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 07/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

protocol ProductMapperProtocol {

  func map(productModel: ProductModel) -> Product

  func map(productListModel: ProductListModel) -> [Product]
}

class ProductMapper: ProductMapperProtocol {

  // MARK: - Constants
  private let coolbluePromoType = "coolblues-choice"
  private let discountPromoType = "action-price"

  func map(productModel: ProductModel) -> Product {
    let id = productModel.productId
    let name = productModel.productName
    let reviewAverage = productModel.reviewInformation.reviewSummary.reviewAverage
    let reviewCount = productModel.reviewInformation.reviewSummary.reviewCount
    let stock = productModel.availabilityState
    let price = productModel.salesPriceIncVat
    let image = productModel.productImage ?? String()
    let isNextDayShippable = productModel.nextDayDelivery

    var promoProduct: PromoProduct?
    if let promoIcon = productModel.promoIcon {
      switch (promoIcon.type) {
      case coolbluePromoType:
        if let detail = productModel.coolbluesChoiceInformationTitle {
          promoProduct = CoolbluePromoProduct(id: id, name: name, reviewAverage: reviewAverage, reviewCount: reviewCount, stock: stock, price: price, image: image, isNextDayShippable: isNextDayShippable, type: ProductType.coolblue, text: promoIcon.text, coolblueDetail: detail)
        }
      case discountPromoType:
        if let discountPrice = productModel.listPriceIncVat,
          let discountPriceNoTax = productModel.listPriceExVat {
          promoProduct = DiscountPromoProduct(id: id, name: name, reviewAverage: reviewAverage, reviewCount: reviewCount, stock: stock, price: price, image: image, isNextDayShippable: isNextDayShippable, type: ProductType.discount, text: promoIcon.text, discountPrice: discountPrice, discountPriceNoTax: discountPriceNoTax)
        }
      default:
        break
      }
    }
    if let promoProduct = promoProduct {
      return promoProduct
    } else {
      let product = Product(id: id, name: name, reviewAverage: reviewAverage, reviewCount: reviewCount, stock: stock, price: price, image: image, isNextDayShippable: isNextDayShippable, type: ProductType.common)
      return product
    }
  }

  func map(productListModel: ProductListModel) -> [Product] {
    var products = [Product]()
    for productModel in productListModel.products {
      let product = map(productModel: productModel)
      products.append(product)
    }
    return products
  }
}

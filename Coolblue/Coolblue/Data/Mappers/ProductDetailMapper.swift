//
//  ProductDetailMapper.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation

protocol ProductDetailMapperProtocol {

  func map(productDetailListModel: ProductDetailListModel) -> ProductDetail
}

class ProductDetailMapper: ProductDetailMapperProtocol {

  func map(productDetailListModel: ProductDetailListModel) -> ProductDetail {
    let productMapper = ProductMapper()
    let productModel = productDetailListModel.product
    let product = productMapper.map(productModel: productModel)
    let productSpecifications = map(specificationSummary: productModel.specificationSummary)
    let productDetail = ProductDetail(id: product.id, name: product.name, reviewAverage: product.reviewAverage, reviewCount: product.reviewCount, stock: product.stock, price: product.price, image: product.image, isNextDayShippable: product.isNextDayShippable, type: product.type, text: productModel.productText, pros: productModel.pros, cons: productModel.cons, images: productModel.productImages, specifications: productSpecifications)
    return productDetail
  }

  private func map(specificationSummary: [SpecificationSummaryModel]) -> [ProductSpecification] {
    var productSpecifications = [ProductSpecification]()
    for specificationSummaryModel in specificationSummary {
      let productSpecification = ProductSpecification(name: specificationSummaryModel.name, value: specificationSummaryModel.value)
      productSpecifications.append(productSpecification)
    }
    return productSpecifications
  }
}

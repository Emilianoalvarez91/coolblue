//
//  StringTransformable.swift
//  Coolblue
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import Foundation
import ObjectMapper

class StringTransformable: TransformType {

  typealias Object = String
  typealias JSON = String

  func transformToJSON(_ value: String?) -> String? {
    guard let value = value else {
      return nil
    }
    return value
  }

  func transformFromJSON(_ value: Any?) -> String? {
    if let stringValue = value as? String {
      return stringValue
    } else if let numberValue = value as? NSNumber {
      return numberValue.stringValue
    } else {
      return nil
    }
  }
}

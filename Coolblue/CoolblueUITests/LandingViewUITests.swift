//
//  LandingViewUITests.swift
//  CoolblueUITests
//
//  Created by Emiliano Alvarez on 08/10/2018.
//  Copyright © 2018 Coolblue. All rights reserved.
//

import XCTest

class LandingViewUITests: XCTestCase {
        
  override func setUp() {
    super.setUp()
    continueAfterFailure = false
    XCUIApplication().launch()
  }
    
  func testKeyboardShouldOpenWhenPressingSearchTabItem() {
    let app = XCUIApplication()
    app.tabBars.buttons["Search"].tap()
    XCTAssert(app.keyboards.count > 0, "The keyboard is not shown")
  }

  func testProductListShouldPopulateAfterSearching() {
    let app = XCUIApplication()
    app.navigationBars["Coolblue.LandingView"].children(matching: .searchField).element.tap()

    let tCapKey = app/*@START_MENU_TOKEN@*/.keys["T"]/*[[".keyboards.keys[\"T\"]",".keys[\"T\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
    tCapKey.tap()

    let eKey = app/*@START_MENU_TOKEN@*/.keys["e"]/*[[".keyboards.keys[\"e\"]",".keys[\"e\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
    eKey.tap()

    let sKey = app/*@START_MENU_TOKEN@*/.keys["s"]/*[[".keyboards.keys[\"s\"]",".keys[\"s\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
    sKey.tap()

    let tKey = app/*@START_MENU_TOKEN@*/.keys["t"]/*[[".keyboards.keys[\"t\"]",".keys[\"t\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
    tKey.tap()

    app.keyboards.buttons["Search"].tap()

    let labels = app.staticTexts["ProductName"]

    let exists = NSPredicate(format: "exists == 1")
    expectation(for: exists, evaluatedWith: labels, handler: nil)
    waitForExpectations(timeout: 5, handler: nil)

    XCTAssertEqual("Apple iPhone 6 32GB Grijs", labels.firstMatch.label)
  }
}
